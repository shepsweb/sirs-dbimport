<?php

namespace Sirs\DBImport;

use App\User;
use DB;
use Illuminate\Console\Command;

class DBImport extends Command
{
    /**
     * The name and signature of the console command. Removed --anonymize and --default-user flags as they were not implemented.
     *
     * @var string
     */
    protected $signature = 'db:import {--demo : Import data from demo database} {--keep : Keep db dump in database/backups} {--local : Import from previously kept backup} {--testonly : Just for testing}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Copies production data to your current database, or copies a different database given flags!';

    /**
     * Tables to ignore
     *
     * @var string
     */

    protected $ignoreTables = null;
    /**
     * Specific Tables to dump
     *
     * @var string
     */

    protected $whitelist = null;

    protected $backupsDir = 'database/backups';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        if (config('dbimport.ignoreTables')) {
            $this->ignoreTables = config('dbimport.ignoreTables');
        }

        if (config('dbimport.whitelist')) {
            $this->whitelist = config('dbimport.whitelist');
        }

        $this->conn = $this->getDefaultConnection();
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (app()->environment('production')) {
            $this->comment('!!!!!!!!!!!!!!WARNING!!!!!!!!!!!!!!');
            if (!$this->confirm('This app is a production environment. Are you sure you want to continue?')) {
                return(0);
            }
        }

        if (config('dbimport.beforeImport')) {
            $this->info('running beforeImport hook...');
            $this->runImportHook(config('dbimport.beforeImport'));
        }


        if ( !$this->option('testonly') ) {
            $file = $this->getFile();
            $this->dropTables();
            $this->importFile($file);
        }

        if (config('dbimport.afterImport')) {
            $this->info('running afterImport hook...');
            $this->runImportHook(config('dbimport.afterImport'));
        }
    }

    /**
     * Creates a backup directory and adds it to .hgignore if it doesn't already exist
     * Connects to the appropriate DB and creates a mysqldump, or finds the latest local dump
     * Returns the filename of the chosen or created file
     * @return string
     */
    public function getFile()
    {
        if (!is_dir($this->backupsDir)) {
            $backupsDir = $this->createBackupsDir();
        }

        if ($this->option('local')) {
            $file = $this->getLastLocalFile();
            $this->info('Using file ' . $file);
        } else {
            $remoteDb = $this->getRemoteConnection();

            if (empty($remoteDb['host']) || empty($remoteDb['database']) || empty($remoteDb['username']) || empty($remoteDb['password'])) {
                $remote = ($this->option('demo')) ? 'Demo' : 'Production';
                $this->error($remote.' DB information in env file is not valid');
                exit();
            }

            $file = $this->backupsDir . '/' . $remoteDb['database'] . "_" . date('Y_m_d_H_i_s') . '.sql';
            $this->info('Dumping data from '.$remoteDb['host'].':'.$remoteDb['database']);
            $exec = "mysqldump --user='%s' --password='%s' -h %s %s";

            // Add options for ignored tables
            if (is_array($this->ignoreTables)) {
                foreach ($this->ignoreTables as $table) {
                    $exec .= ' --ignore-table=' . $remoteDb['database'] . '.' . $table . ' ';
                }
            }

            // Add options for whitelist
            if (is_array($this->whitelist)) {
                foreach ($this->whitelist as $table) {
                    $d .= " " . $table;
                }
            }
            $exec .= "> %s";
            $command = (sprintf($exec, $remoteDb['username'], $remoteDb['password'], $remoteDb['host'], $remoteDb['database'], $file));
            exec($command, $output, $return_value);
            if ($return_value !== 0) {
                unlink($file);
                $this->error("Failed to connect to Production Database");
                exit();
            }
            $this->info('Created file ' . $file);
        }

        return $file;
    }

    /**
     * Queries the current DB for the names of all tables
     * Iterates over table names and drops each of them
     * @return nothing
     */
    public function dropTables()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        $tables = DB::select('SHOW TABLES');
        $attribute = "Tables_in_".$this->conn['database'];

        // Only remove tables from whitelist
        if (is_array($this->whitelist)) {
            foreach ($tables as $key => $table) {
                if (!in_array($table->$attribute, $this->whitelist)) {
                    unset($tables[$key]);
                }
            }
        }

        $this->info('Dropping tables...');
        $bar = $this->output->createProgressBar(count($tables));
        foreach ($tables as $table) {
            if (DB::statement('DROP TABLE '. $table->$attribute)) {
                $bar->advance();
            } else {
                $this->error("Failed to drop table " . $table->$attribute);
                exit();
            }
        }
        $bar->finish();
        $this->info('');
        $this->info('Successfully dropped all tables');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }

    public function importFile($file)
    {
        $importMethod = config('dbimport.import-method');
        $this->$importMethod($file);
        // $this->importFileWithDB($file);
        // $this->importFileWithMysqlDump($file);
    }

    /**
     * Takes a file name, gets the file and imports it into the current database
     * Deletes the file if the "keep" flag is not set
     * @return string
     */
    public function importFileWithMysqlDump($file)
    {
        $this->info('Importing with mysqldump...');
        // Get local db connection info from .env file

        /* Importing mysql file and deleting it given flags */
        $this->info('Importing data...');
        $exec = "mysql --user='%s' --password='%s' -f -h %s %s < %s";
        $command = (sprintf($exec, $this->conn['username'], $this->conn['password'], $this->conn['host'], $this->conn['database'], $file));
        exec($command, $output, $return_value);

        if ($return_value !== 0) {
            unlink($file);
            $this->error("Failed to connect to Local Database");
            exit();
        }

        $this->info('Imported file ' . $file);
        if ($this->option('local') == false) {
            if ($this->option('keep')== false) {
                unlink($file);
                $this->info('Removed file ' . $file);
            }
        }
    }

    private function importFileWithDB($file)
    {
        $this->info('Importing with Laravel DB facade...');
        $fh = fopen($file, 'r');

        $statement = '';
        while (!feof($fh)) {
            $line = fgets($fh);
            if (substr($line, 0, 2) == '--') {
                continue;
            }
            if (substr(trim($line), -1, 1) == ';') {
                $statement .= trim($line);
                $this->info('running "'.substr($statement, 0, 64).'..."');
                DB::unprepared($statement);
                $statement = '';
                continue;
            };
            $statement .= trim($line);
        };
        fclose($fh);
    }
    
    private function getLastLocalFile()
    {
        $backups = scandir($this->backupsDir);
        if (count($backups) == 2) {
            $this->error('No local backups found');
            exit();
        }
        $latest = $backups[count($backups) - 1];
        return $this->backupsDir . '/' . $latest;
    }

    private function createBackupsDir()
    {
        mkdir($this->backupsDir, 0755, true);
        if (file_exists(base_path('.hgignore'))) {
            $path = base_path('.hgignore');
        } elseif (file_exists(base_path('.gitignore'))) {
            $path = base_path('.gitignore');
        }
        $ignoreLines = explode("\n", file_get_contents($path));
        if (!in_array($this->backupsDir, $ignoreLines)) {
            $ignoreLines[] = $this->backupsDir;
        }
        file_put_contents($path, implode("\n", $ignoreLines));
    }

    private function getRemoteConnection()
    {
        if ($this->option('demo')) {
            return [
                'host' =>  config('dbimport.demo_db.host'),
                'database' =>  config('dbimport.demo_db.database'),
                'username' =>  config('dbimport.demo_db.username'),
                'password' =>  config('dbimport.demo_db.password'),
            ];
        }

        return [
            'host' =>  config('dbimport.prod_db.host'),
            'database' =>  config('dbimport.prod_db.database'),
            'username' =>  config('dbimport.prod_db.username'),
            'password' =>  config('dbimport.prod_db.password'),
        ];
    }
    
    private function getDefaultConnection()
    {
        return config('database.connections.'.config('database.default'));
    }

    private function runImportHook($action)
    {
        if (!$action) {
            return;
        }
        
        if (strpos($action, '@') !== false) {
            list($class, $method) = explode('@', $action);

            if (class_exists($class) && method_exists($class, $method)) {
                forward_static_call([$class, $method]);
                return;
            }
            throw new \Exception('Your import hook '.$action.' does not exist');
        }
        
        call_user_func($action);
    }
}
