<?php

namespace Sirs\DBImport;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Routing\Router;

use Config;

class DBImportServiceProvider extends ServiceProvider
{

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // register the command
        $this->commands([
            DBImport::class
        ]);
        $this->registerMyPackage();
    }


  
    
    /**
     * Bootstrap the Tasks services.
     *
     * @return void
     */
    public function boot(Router $router)
    {
        $this->publishes([ __DIR__.'/config.php' => config_path('dbimport.php') ], 'config');
        $this->publishes([ __DIR__.'/../../../ImportHooks.php' => app_path('DbImport/ImportHooks.php') ], 'import-hooks');

        // Register the command if we are using the application via the CLI
        if ($this->app->runningInConsole()) {
            $this->commands([
                DBImport::class,
            ]);
        }
    }
    private function registerMyPackage()
    {
        $this->app->bind('DBImport',function($app){
            return new DBImport($app);
        });

    }
}
