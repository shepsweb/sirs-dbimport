# Sirs-DBImport #
Package for importing data from live of demo database.

## Contents ##
* DBImport - artisan command for importing data.

## Installation & Configuration ##
Add sirs-dbimport to composer.json
```
    composer require sirs/dbimport
```

Add the service provider to config/app.php:
```
    ...
    Sirs\DBImport\DBImportServiceProvider::class,
    ...
```

Publish the config file:
```
php artisan vendor:publish
```

Add `beforeImport` and/or 'afterImport' functions.

Add database info to your .env file
```
# DB Host and Auth infor for DBImport command.
PROD_DB_HOST=your.prod.host.name
PROD_DB_DATABASE=prod_db_name
PROD_DB_USERNAME=prod_db_username
PROD_DB_PASSWORD=prod_password

DEMO_DB_HOST=your.demo.host.name
DEMO_DB_DATABASE=demo_db_name
DEMO_DB_USERNAME=demo_db_username
DEMO_DB_PASSWORD=demo_password
```



## Usage ##
To import: `php artisan db:import [options]`

### Configs ###
* beforeImport: a static action ('class@method') that is called before the db dump is retreived.
* afterImport: a static action ('class@method') that is called after the data has been imported into the local database.
* ignoreTables: array of tables to skip in the import

### Options: ###
* **--demo**: Import data from demo database
* **--keep**: Keep db dump in database/backups
* **--local**: Import from previously kept backup

### Docker ###
If you are using this in docker, you will need the mysqldump program in order to actually use it (NOT required to just have it in your composer.json).
```
apt-get install default-mysql-client
```
