# Change Log

## 1.1.0 2016-11-11
* Added config that publishes to dbimport.
* Added support for beforeImport and afterImport hooks.  You can define these functions in configs/dbimport.php.  See the default config for an example.

