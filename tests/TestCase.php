<?php

namespace Sirs\DBImport\Tests;
//use PHPUnit\Framework\TestCase as PHPUnitTestCase;
//use Orchestra\Testbench\TestCase as OrchestraTestCase;   //OrchestraTestCase
use Sirs\Anonymizer\AnonymizerServiceProvider;
use Sirs\DBImport\DBImportServiceProvider;
use Sirs\DBImport\DbImport;

class TestCase extends \Orchestra\Testbench\TestCase
{
    protected $loadEnvironmentVariables = true;
    //

    /**
	 * Get package providers.
	 *
	 * @param  \Illuminate\Foundation\Application  $app
	 *
	 * @return array
	 */
	protected function getPackageProviders($app)
	{
	    return [
	        \Sirs\DBImport\DBImportServiceProvider::class
	    ];
	}

	public function testSomethingIsTrue()
    {
        $obj = new DBImport;
        $this->assertTrue(true);
    }

    //protected $enablesPackageDiscoveries = true; 

}
