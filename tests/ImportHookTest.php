<?php

namespace Sirs\DBImport\Tests;

use Sirs\DBImport\Tests\TestCase;

class ImportHookTest extends TestCase
{
    // Test before and after hooks in here


    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $this->assertTrue(true);
    }

    /**
	 * Test a console command.
	 *
	 * @return void
	 */
	public function test_import_hook_test()
	{
		$this->assertTrue(true);

		\Config::set('dbimport.beforeImport', '\Sirs\DBImport\Tests\ImportHookTest@beforeImport');

		\Config::set('dbimport.afterImport', 'Sirs\DBImport\Tests\ImportHookTest@afterImport');


	    $this->artisan('db:import --testonly')
	         ->expectsOutput('running beforeImport hook...')
	         ->expectsOutput('running afterImport hook...')
	         ->assertExitCode(0);
	}

	/**
	 * Test a console command.
	 *
	 * @return void
	 */
	public function test_import_hook_test_off()
	{
		$this->assertTrue(true);

		

	    $this->artisan('db:import --testonly')
	         ->doesntExpectOutput('running beforeImport hook...')
	         ->doesntExpectOutput('running afterImport hook...')
	         ->assertExitCode(0);
	}

	public static function beforeImport()
    {
        // code here
    }

    public static function afterImport()
    {
        // code here
    }
}
