<?php

namespace Tests;

use Illuminate\Support\Facades\Artisan;
use Sirs\DBImport\Tests\TestCase;

class ProductionPromptTest extends TestCase
{
    public function testProductionEnvironmentPromptFalse()
    {
        app()->detectEnvironment(function () {
            return 'demo';
        });

        $this->assertTrue(true);

        $this->artisan('db:import --testonly')
            ->doesntExpectOutput('!!!!!!!!!!!!!!WARNING!!!!!!!!!!!!!!')
            ->assertExitCode(0);
    }

    public function testProductionEnvironmentPrompt()
    {
        app()->detectEnvironment(function () {
            return 'production';
        });

        $this->assertTrue(true);

        $this->artisan('db:import --testonly')
            ->expectsOutput('!!!!!!!!!!!!!!WARNING!!!!!!!!!!!!!!')
            ->expectsQuestion('This app is a production environment. Are you sure you want to continue?', false)
            ->assertExitCode(0);
    }
}
